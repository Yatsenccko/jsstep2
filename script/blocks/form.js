import Input from "../elements/input.js"
import Button from "../elements/button.js"
import Select from "../elements/select.js"
import { doctorsSelect, urgencySelect }  from "../config/configSelect.js"
import { emailInput, passwordInput, purposeInput, descriptionInput, filterInput, firstNameInput, secondNameInput, surNameInput} from "../config/configInputs.js"
import { btnLogin, btnCancel } from "../config/configButtons.js"
import API from "../API/API.js"
import { btnFilter } from "../config/configButtons.js"
import { filterPriority, filterStatus } from "../config/configSelects.js"


class Form {
  //creating form on page
  constructor(onClose) {
    this.form = document.createElement("form")
    this.onClose = onClose
  }

  //creating form for login 
  renderLoginForm() {
    const email = new Input(emailInput).render()
    const password = new Input(passwordInput).render()
    const buttonLogin = new Button(btnLogin).render()
    const buttonCancel = new Button(btnCancel).render(this.onClose)
    this.form.append(email, password, buttonCancel, buttonLogin)
    this.form.addEventListener("submit", this.handleLoginForm.bind(this))
    return this.form
  }
  
  //taking data of all inputs n write it in formData
  async handleLoginForm(event) {
    event.preventDefault()
    const form = event.target.closest("form")
    const input = form.querySelectorAll("input")
    const formData = {}
    input.forEach((item) => {
      formData[item.name] = item.value
    })
    const token = await API.getDataToken("/cards/login", formData)
    console.log(token);
    if (token) {
      const data = await API.getData("/cards")
      this.onClose(event)
    }
  }

  
  renderCreateCard(){
    const doctors = new Select(doctorSelect).render();
    this.form.append(doctors)
    const inputPurpose = new Input(purposeSelect).render();
    this.form.append(inputPurpose)
    const description = new Input(desc).render();
    this.form.append(description)
    const inputUrgency = new Select(urgency).render();
    this.form.append(inputUrgency)
    const inputName = new Input(name).render();
    this.form.append(inputName)
    const inputSecondName = new Input(secondName).render();
    this.form.append(inputSecondName)
    const inputSurname = new Input(surName).render();
    this.form.append(inputSurname)
    document.getElementById("root").append(this.form)
    return this.form
  }

  renderFilter() {
    const searchBtn = new Button(filterButton).render()
    const filterInput = new Input(filter).render()
    const filterStat = new Select(filterStatus).render()
    const filterPrior = new Select(filterPriority).render()
    this.form.append(filterInput, filterStat, filterPrior, searchBtn)
    this.form.addEventListener("submit", this.filter.bind(this))
    return this.form

  }
  
  filter(event) {
    event.preventDefault()
    const form = event.target.closest("form")
    const input = form.querySelectorAll("input")
    const select = form.querySelectorAll("select")
    const formDataSelect = [...input.reduce((acc, curr) => {
      acc[curr.name] = curr.value
    }, {})]
    const formData = [...input.reduce((acc, curr) => {
      acc[curr.name] = curr.value
    }, {})]
    const newestFormData = { ...formData, ...formDataSelect }
  }

}



export default Form