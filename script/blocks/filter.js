import Inputs from "../elements/input.js"
import Button from "../elements/button.js"
import Select from "../elements/select.js"
import Form from "../blocks/form.js"

class Filter {

    constructor() {
        this.div = document.createElement("div")
    }
    render() {
        const form = new Form().renderFilter()
        this.div.append(form)
        return this.div
    }
}

export default Filter