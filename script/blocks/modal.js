import Form from "../blocks/form.js"

class Modal {
    //creating modal's id body title n also modal 
    constructor({ id, body, title }) {
        this.id = id
        this.body = body
        this.title = title
        this.modal = document.createElement("div")
    }
    //appropriation modal's id body title n returning created modal
    renderLogin() {
        this.modal.id = this.id
        this.modal.className = "modal"
        this.modal.innerHTML = this.body
        const title = document.createElement("h3")
        title.textContent = this.title
        const form = new Form(this.closeModal.bind(this)).renderLoginForm()
        const closeBtn = this.modal.querySelector(".close")
        closeBtn.addEventListener("click", this.closeModal.bind(this))
        const modalContent = this.modal.querySelector(".modal-content")
        modalContent.append(form)
        return this.modal
    }
    //binding to close modal
    closeModal(event) {
        console.log(event)
        const parent = event.target.closest(".modal")
        parent.remove()
    }
}
export default Modal
