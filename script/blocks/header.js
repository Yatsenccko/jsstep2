import Modal from "../blocks/modal.js"
import Button from "../elements/button.js"
import Form from "../blocks/form.js"
import { btnHeader } from "../config/configButtons.js"
import { modalConfig } from "../config/configModal.js"
import { btnCreateCard } from "../config/configButtons.js"

class Header {
    //creating header
    constructor() {
        this.header = document.createElement("header")
        this.container = document.createElement("div")
    }
    //creating logo putting button and returning created header
    renderBase() {
        this.container.className = "container header-wrapper"
        const img = document.createElement("img")
        img.src = "./imgs/doctor.png"
        img.alt = "logo"
        this.container.prepend(img)
        this.header.append(this.container)
        return this.header
    }

    // Without Login
    renderUnlogged() {
        const btn = new Button(btnHeader).render(this.modalOpen.bind(this))
        this.container.append(btn)
        this.renderBase()
        return this.header
    }
    modalOpen() {
        const parent = document.getElementById("root")
        const modal = new Modal(modalConfig).renderLogin()
        parent.append(modal)
    }
    
    // Login present
    renderLoggedIn() {
        const btn = new Button(createCard).render(this.openModalCard.bind(this))
        this.renderBase()
        this.container.append(btn)
        return 123
    }
    openModalCard() {
        const form = new Modal().renderCard();
    }

    
}

export default new Header