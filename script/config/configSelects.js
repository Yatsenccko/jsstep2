//export selector's configuration 
export const selectDoctor = {
    name:"Doctor",
    className:"inputs",
    option:["doctor","cardiolog","terapeft","dentist"],
    text:["Doctor","Cardiolog","Terapevt","Dentist"]
}
export const filterStatus = {
    name:"filterStatus" ,
    className:"filter",
    option:["status","done","await"],
    text:["status","done","await"]
}
export const filterPriority = {
    name:"filterPriority" ,
    className:"filter",
    option:["priority","high","low","normal"],
    text:["priority","high","low","normal"]
}