//export  config for each button 
export const btnHeader = {
    text: "Login",
    type: "button",
    className: "btn btn-primary"
}
export const btnLogin = {
    text: "Submit",
    type: "button",
    className: "submit"
}
export const btnCancel = {
    text: "Cancel",
    type: "button",
    className: "cancel"
}
export const btnFilter = {
    text: "Search",
    type: "submit",
    className: "search"
}
export const btnCreateCard = {
    text: "Create Card",
    type: "button",
    className: "createCard"
}
export const btnExit = {
    text: "Exit",
    type: "button",
    className: "exitBtn"
}
