//export config for each input 
export const emailInput = {
    placeholder: "email",
    type: "email",
    name: "email",
    className: "inputs",
}

export const passwordInput = {
    placeholder: "password",
    type: "password",
    name: "password",
    className: "inputs",
}

export const filterInput = {
    type: "submit",
    placeholder: "find doctor",
    name: "filter",
    className: "filter"
}

export const purposeInput = {
    type: "text",
    placeholder: "Purpose of the visit",
    name: "purpose",
    className: "purpose"
}

export const descriptionInput = {
    type: "text",
    placeholder: "write about your problem",
    name: "description",
    className: "desc",
}

export const firstNameInput = {
    type: "text",
    placeholder: "Your name",
    name: "name",
    className: "name",
}

export const secondNameInput = {
    type: "text",
    placeholder: "Your secondname",
    name: "secondName",
    className: "secondName",
}

export const surNameInput = {
    type: "text",
    placeholder: "Your surname",
    name: "surName",
    className: "surName",
}
export const btnLogin = {
    type: "submit",
    name: "submit",
    className: "filter"
}
