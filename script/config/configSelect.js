//export selector's configuration 
export const doctorsSelect = {
    name:"Doctor",
    className:"inputs",
    option:["doctor","cardiolog","terapeft","dentist"],
    text:["Doctor","Cardiolog","Terapevt","Dentist"]
}
export const urgencySelect = {
    name:"urgency",
    className:"urgency",
    option:["Urgency","normal","preoretic","urgent"],
    text:["Urgency","normal","preoretic","urgent"]
}

export const filterStatusSelect = {
    name:"filterStatus" ,
    className:"filter",
    option:["status","done","await"],
    text:["status","done","await"]
}
export const filterPrioritySelect = {
    name:"filterPriority" ,
    className:"filter",
    option:["priority","high","low","normal"],
    text:["priority","high","low","normal"]
}