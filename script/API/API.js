//token a308511d-5626-4faf-8167-68a53cb46e93
class API {
  constructor() {
    //baseurl API for work
    this.baseUrl = "https://ajax.test-danit.com/api/v2"
    //check token in local storage token or null
    this.token = this.getToken() || null
  }
  //take string with part of url fetch request baseurl+url,method GET,bearer token from constructor if res = ok return all cards
  async getData(url) {
    const res = await fetch(`https://ajax.test-danit.com/api/v2/cards/login`, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      },
    })
    if (res.ok) {
      return res.json()
    }
  }
  //take string with part of url and object with info about card to create ,method POST ,bearer token from constructor,body = object to create , if res = ok return created data with id
  async createData(url, data) {
    console.log(this.token)
    const res = await fetch(`${this.baseUrl}${url}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      },
      body: JSON.stringify({
        title: 'Визит к кардиологу',
        description: 'Плановый визит',
        doctor: 'Cardiologist',
        bp: '24',
        age: 23,
        weight: 70
      })

    })
    console.log(data)
    if (res.ok) {
      return res.json()
    }
  }
  //take string with part of url n id,method DELETE,bearer token from constructor,if res = ok return message
  async deleteData(url, id) {
    const res = await fetch(`${this.baseUrl}${url}/${id}`, {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      }
    })
    if (res.ok) {
      return res
    }

  }
  //take string with part of url n id n object to edit ,body = edited data,method PUT,bearer token from constructor,if res = ok return edited data
  async editData(url, id, data) {
    const res = await fetch(`${this.baseUrl}${url}/${id}`, {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      },
      body: JSON.stringify(data)
    })
    if (res.ok) {
      return res.json()
    }
  }
  //take string with part of url n id,method GET,bearer token from constructor , if res= ok return data by id
  async getDataById(url, id) {
    const res = await fetch(`${this.baseUrl}${url}/${id}`, {
      headers: {
        'Authorization': `Bearer ${this.token}`
      }
    })
    if (res.ok) {
      return res.json()
    }
  }
  //takes nothing,taking token in local storage and return token or null 
  getToken() {
    const token = localStorage.getItem("token")
    if (token) {
      this.token = token

    }
    return this.token
  }
  //take string with part of url n email n password ,method POST,if res = ok puts token in localStorage n return token
  async getDataToken(url, data) {
    console.log(url);
    const res = await fetch(`${this.baseUrl}${url}`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    })
    if (res.ok) {
      const token = await res.text()
      localStorage.setItem("token", token)
      this.token = token
      return this.token
    }
    return undefined
  }
}

export default new API()