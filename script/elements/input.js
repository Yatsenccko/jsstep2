//creating input className type placeholder n name 
class Input {
  constructor({ className, type, placeholder, name }) {
    this.input = document.createElement("input")
    this.className = className
    this.type = type
    this.placeholder = placeholder
    this.name = name
  }
  //appropriation className type placeholder n name to input
  render() {
    this.input.className = this.className
    this.input.type = this.type
    this.input.placeholder = this.placeholder
    this.input.name = this.name
    return this.input
  }
}
export default Input