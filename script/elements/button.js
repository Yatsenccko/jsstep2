//creating buttons className type n text and also button 
class Button {
  constructor({ className, type, text }) {
    this.button = document.createElement("button")
    this.className = className
    this.type = type
    this.text = text
  }
  // setting text eventlistener type className on our button 
  render(onClick) {
    this.button.className = this.className
    this.button.type = this.type
    this.button.addEventListener("click", onClick)
    this.button.textContent = this.text
    return this.button
  }
}
export default Button