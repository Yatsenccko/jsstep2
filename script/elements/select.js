//creating selector's name,className,option,text
class Select {
    constructor({ name, className, option, text }) {
        this.name = name
        this.className = className
        this.option = option
        this.text = text
        this.select = document.createElement("select")
    }
    //appropriation selector's name,className,option,text
    render() {
        this.select.name = this.name
        this.select.className = this.className
        this.select.insertAdjacentHTML("beforeend", `${this.option.map((item, index) => {
            if (!index) {
                return `<option disabled >${this.text[index]}</option>`
            }
            return `<option value=${item}>${this.text[index]}</option>`
        }).join("")}   `)

        return this.select
    }

}
export default Select